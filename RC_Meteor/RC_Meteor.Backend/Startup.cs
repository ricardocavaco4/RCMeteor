﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RC_Meteor.Backend.Startup))]
namespace RC_Meteor.Backend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
