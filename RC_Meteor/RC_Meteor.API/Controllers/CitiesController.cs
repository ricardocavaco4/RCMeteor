﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace RC_Meteor.API.Controllers
{

    public class CitiesController : ApiController
    {

        private DataClassesDataContext db = new DataClassesDataContext();

        // GET: api/Cities
        public List<City> Get()
        {
            return db.Cities.ToList();
        }

        // GET: api/Cities
        [ResponseType(typeof(City))]
        public IHttpActionResult Get(string country)
        {

            if (string.IsNullOrWhiteSpace(country))
            {
                return NotFound();
            }

            var find = db.Cities.Where(c => c.country == country);

            if (find.Count() == 0)
            {
                return NotFound();
            }

            return Ok(find);

        }

       
    }
}
