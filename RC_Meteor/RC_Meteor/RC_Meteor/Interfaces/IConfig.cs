﻿namespace RC_Meteor.Interfaces
{

    using SQLite.Net.Interop;

    public interface IConfig
    {

        string DirectoryDB { get; }

        ISQLitePlatform Platform { get; }

    }

}
