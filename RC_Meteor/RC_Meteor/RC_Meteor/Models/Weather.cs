﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace RC_Meteor.Models
{

    /// <summary>
    /// Todos os tipos de tempo.
    /// </summary>
    public class Weather
    {

        [JsonProperty(PropertyName = "id")]
        [PrimaryKey]
        public int WeatherID { get; set; }

        [JsonProperty(PropertyName = "main")]
        public string WeatherCondition { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "icon")]
        public string IconID { get; set; }

        public override int GetHashCode()
        {
            return WeatherID;
        }

    }

}
