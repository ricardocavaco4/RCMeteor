﻿using GalaSoft.MvvmLight.Command;
using Newtonsoft.Json;
using RC_Meteor.ViewModels;
using RC_Meteor.Views;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace RC_Meteor.Models
{
    public class Country
    {


        [PrimaryKey]
        [AutoIncrement]
        public int CountryID { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "alpha2Code")]
        public string Alpha2Code { get; set; }

        [JsonProperty(PropertyName = "alpha3Code")]
        public string Alpha3Code { get; set; }

        [JsonProperty(PropertyName = "capital")]
        public string Capital { get; set; }

        [JsonProperty(PropertyName = "region")]
        public string Region { get; set; }

        [JsonProperty(PropertyName = "numericCode")]
        public string NumericCode { get; set; }

        public ICommand SelectCountryCommand
        {
            get { return new RelayCommand(SelectCountry); }
        }

        private async void SelectCountry()
        {

            MainViewModel.GetInstance().CitiesList = new List<City>();
            MainViewModel.GetInstance().Cities = new CitiesViewModel(this);

            await Application.Current.MainPage.Navigation.PushAsync(new CitiesListPage());

        }

        public override int GetHashCode()
        {
            return this.CountryID;
        }

    }

}
