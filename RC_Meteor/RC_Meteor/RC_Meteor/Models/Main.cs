﻿namespace RC_Meteor.Models
{
    using Newtonsoft.Json;
    using SQLite;
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Os dados principais.
    /// </summary>
    public class Main
    {

        [PrimaryKey]
        [AutoIncrement]
        public int MainID { get; set; }

        [JsonProperty(PropertyName = "temp")]
        public double? CurrentTemperature { get; set; }

        [JsonProperty(PropertyName = "temp_min")]
        public double? MinTemperature { get; set; }

        [JsonProperty(PropertyName = "temp_max")]
        public double? MaxTemperature { get; set; }

        [JsonProperty(PropertyName = "pressure")]
        public double? Pressure { get; set; }

        [JsonProperty(PropertyName = "sea_level")]
        public double? PressureSeaLevel { get; set; }

        [JsonProperty(PropertyName = "grnd_level")]
        public double? PressureGroundLevel { get; set; }

        [JsonProperty(PropertyName = "humidity")]
        public int? Humidity { get; set; }

        [JsonProperty(PropertyName = "temp_kf")]
        public double? temp_kf { get; set; }

        public override int GetHashCode()
        {
            return this.MainID;
        }

    }

}
