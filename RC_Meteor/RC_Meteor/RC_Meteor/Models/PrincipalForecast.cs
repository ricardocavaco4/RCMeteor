﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace RC_Meteor.Models
{

    public class PrincipalForecast
    {

        [PrimaryKey]
        [AutoIncrement]
        public int PrincipalID { get; set; }

        [JsonProperty(PropertyName = "cod")]
        public string Code { get; set; }

        [JsonProperty(PropertyName = "message")]
        public double Message { get; set; }

        [JsonProperty(PropertyName = "cnt")]
        public int? SurroundingCitiesNumber { get; set; }

        [JsonProperty(PropertyName = "list")]
        public List<List> List { get; set; }

        [JsonProperty(PropertyName = "city")]
        public City City { get; set; }

        public override int GetHashCode()
        {
            return this.PrincipalID;
        }

    }

}
