﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace RC_Meteor.Models
{
    public class User
    {

        [PrimaryKey]
        [AutoIncrement]
        public int UserID { get; set; }

        public string UserEmail { get; set; }

        public string Password { get; set; }

        public override int GetHashCode()
        {
            return this.UserID;
        }

    }

}
