﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace RC_Meteor.Models
{

    public class Wind
    {

        [JsonProperty(PropertyName = "speed")]
        public double? Speed { get; set; }

        [JsonProperty(PropertyName = "deg")]
        public double? Direction { get; set; }

    }
}

