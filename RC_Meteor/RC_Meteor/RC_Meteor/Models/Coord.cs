﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace RC_Meteor.Models
{
    public class Coord
    {

        [PrimaryKey]
        [AutoIncrement]
        public int CoordinatesID { get; set; }

        [JsonProperty(PropertyName = "lat")]
        public double? Latitude { get; set; }

        [JsonProperty(PropertyName = "lon")]
        public double? Longitude { get; set; }

        public override int GetHashCode()
        {
            return this.CoordinatesID;
        }

    }
}
