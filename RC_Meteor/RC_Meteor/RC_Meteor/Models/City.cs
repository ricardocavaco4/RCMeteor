﻿using GalaSoft.MvvmLight.Command;
using Newtonsoft.Json;
using RC_Meteor.Services;
using RC_Meteor.ViewModels;
using RC_Meteor.Views;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace RC_Meteor.Models
{

    public class City
    {

        [JsonProperty(PropertyName = "id")]
        [PrimaryKey]
        public int CityID { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "country")]
        public string Country { get; set; }
        
        [JsonProperty(PropertyName = "population")]
        public int Population { get; set; }

        public ICommand SelectCityCommand
        {
            get { return new RelayCommand(SelectCity); }
        }

        private async void SelectCity()
        {

            MainViewModel main = MainViewModel.GetInstance();

            ApiService _api = new ApiService();

            var connection = await _api.CheckConnection();

            if (!connection.IsSuccess)
            {

                await Application.Current.MainPage.DisplayAlert("Erro", "Por favor verifique a sua ligação à internet!", "OK");
                return;

            }

            var response = await _api.GetWeather<PrincipalForecast>("https://api.openweathermap.org", "/data/2.5", "/forecast?q=" + this.Name + "&units=metric&APPID=0925d6762b9b8780cb7552e9d5b57a9c");

            if (!response.IsSuccess)
            {

                await Application.Current.MainPage.DisplayAlert("Erro", response.Message, "Ok");
                return;

            }

            main.Forecast = (PrincipalForecast)response.Result;

            main.Forecasts = new ForecastViewModel();

            await Application.Current.MainPage.Navigation.PushAsync(new ForecastTabbedPage());

        }

        public override int GetHashCode()
        {
            return this.CityID;
        }

    }

}
