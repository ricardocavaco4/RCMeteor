﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace RC_Meteor.Models
{

    /// <summary>
    /// Percentagem de visibilidade
    /// </summary>
    public class Clouds
    {

        [PrimaryKey]
        [AutoIncrement]
        public int CloudID { get; set; }

        [JsonProperty(PropertyName = "all")]
        public int? Cloudiness { get; set; }

        public override int GetHashCode()
        {
            return this.CloudID;
        }

    }

}
