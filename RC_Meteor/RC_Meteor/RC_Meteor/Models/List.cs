﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using SQLite;

namespace RC_Meteor.Models
{

    public class List
    {


        [PrimaryKey]
        [AutoIncrement]
        public int ListID { get; set; }

        [JsonProperty(PropertyName = "dt")]
        public int? DateTimeNumeric { get; set; }

        [JsonProperty(PropertyName = "main")]
        public Main Main { get; set; }

        [JsonProperty(PropertyName = "weather")]
        public List<Weather> WeatherList { get; set; }

        [JsonProperty(PropertyName = "clouds")]
        public Clouds Clouds { get; set; }

        [JsonProperty(PropertyName = "wind")]
        public Wind Wind { get; set; }

        [JsonProperty(PropertyName = "dt_txt")]
        public string DateTimeString { get; set; }

        public override int GetHashCode()
        {
            return this.ListID;
        }

    }

}
