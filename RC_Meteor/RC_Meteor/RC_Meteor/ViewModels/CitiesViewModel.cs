﻿
namespace RC_Meteor.ViewModels
{

    using GalaSoft.MvvmLight.Command;
    using RC_Meteor.Models;
    using RC_Meteor.Services;
    using RC_Meteor.Views;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class CitiesViewModel : BaseViewModel
    {

        #region Attributes

        private MainViewModel _main;
        private ApiService _api;
        private string _filter;
        private bool _isRefreshing;
        private ObservableCollection<City> _cities;

        #endregion

        #region Properties

        public bool IsRefreshing
        {
            get { return this._isRefreshing; }
            set { SetValue(ref this._isRefreshing, value); }
        }

        public string Filter
        {
            get { return this._filter; }
            set
            {
                SetValue(ref this._filter, value);
                this.Search();
            }
        }

        public ObservableCollection<City> Cities
        {
            get { return this._cities; }
            set { SetValue(ref this._cities, value); }
        }

        public Country Country { get; set; }

        #endregion

        #region Constructor

        public CitiesViewModel(Country country)
        {
            this.Country = country;
            this._main = MainViewModel.GetInstance();
            this._api = new ApiService();
            this.LoadCities();

        }

        #endregion

        #region Commands

        public ICommand RefreshCommand
        {
            get { return new RelayCommand(LoadCities); }
        }

        public ICommand SearchCommand
        {
            get { return new RelayCommand(Search); }
        }

        #endregion

        #region Command Methods

        private void Search()
        {

            if (string.IsNullOrWhiteSpace(this.Filter))
            {
                this.Cities = new ObservableCollection<City>(this.ToCity());
            }
            else
            {
                this.Cities = new ObservableCollection<City>(this.ToCity().Where
                (c => c.Name.ToLower().Contains(this.Filter.ToLower()) ||
                 c.Country.ToLower().Contains(this.Filter.ToLower())
                ));
            }

        }

        #endregion

        #region Methods

        private IEnumerable<City> ToCity()
        {

            return _main.CitiesList.Select(c => new City
            {
                CityID = c.CityID,
                Name = c.Name,
                Population = c.Population,
                Country = c.Country,

            });

        }

        private async void LoadCities()
        {

            IsRefreshing = true;

            var connection = await this._api.CheckConnection();

            if (!connection.IsSuccess)
            {

                await Application.Current.MainPage.DisplayAlert("Erro", connection.Message, "Ok");

                await Application.Current.MainPage.Navigation.PopAsync();

                return;

            }

            var response = await this._api.GetList<City>("http://rcmeteorapi.azurewebsites.net", "/api", "/Cities?country=" + Country.Alpha2Code);

            if (!response.IsSuccess)
            {

                await Application.Current.MainPage.DisplayAlert("Erro", response.Message, "Ok");
                return;

            }

            this._main.CitiesList = (List<City>)response.Result;

            this.Cities = new ObservableCollection<City>(this.ToCity());

            IsRefreshing = false;

        }

        #endregion

    }

}
