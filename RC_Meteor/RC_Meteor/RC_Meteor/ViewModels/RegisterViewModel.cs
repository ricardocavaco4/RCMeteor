﻿namespace RC_Meteor.ViewModels
{

    using RC_Meteor.Services;
    using Xamarin.Forms;

    public class RegisterViewModel : BaseViewModel
    {

        #region Attributes

        private string _source;

        #endregion

        #region Properties

        public string Source
        {
            get { return this._source; }
            set { SetValue(ref this._source, value); }
        }

        #endregion

        #region Constructor

        public RegisterViewModel()
        {

            this.Source = "http://rcmeteorbackend.azurewebsites.net/Account/RegisterMobile";

        }

        #endregion

    }
}
