﻿using GalaSoft.MvvmLight.Command;
using RC_Meteor.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace RC_Meteor.ViewModels
{

    public class ProfileViewModel : BaseViewModel
    {

        #region Attributes

        private MainViewModel _main;
        private ApiService _api;
        private bool _isVisible;
        private string _newpass, _confirmpass, _email, _password;

        #endregion

        #region Properties

        public bool IsVisible
        {
            get { return this._isVisible; }
            set { SetValue(ref this._isVisible, value); }
        }

        public string NewPassword
        {
            get { return this._newpass; }
            set { SetValue(ref this._newpass, value); }
        }

        public string ConfirmPassword
        {
            get { return this._confirmpass; }
            set { SetValue(ref this._confirmpass, value); }
        }

        public string Email
        {
            get { return this._email; }
            set { SetValue(ref this._email, value); }
        }

        public string Password
        {
            get { return this._password; }
            set { SetValue(ref this._password, value); }
        }

        #endregion

        #region Constructor

        public ProfileViewModel()
        {

            this._main = MainViewModel.GetInstance();
            this._api = new ApiService();
            this.IsVisible = false;
            this.Email = _main.Login.Email;
            this.Password = _main.Login.Password;

        }

        #endregion

        #region Commands

        public ICommand ChangeCommand
        {
            get { return new RelayCommand(Change); }
        }

        public ICommand VisibilityCommand
        {
            get { return new RelayCommand(Visibility); }
        }

        #endregion

        #region Command Methods

        private async void Change()
        {

            if(string.IsNullOrWhiteSpace(this.NewPassword) || string.IsNullOrWhiteSpace(this.ConfirmPassword))
            {

                await Application.Current.MainPage.DisplayAlert("Erro", "Tem de preencher todos os campos!", "Ok");

                return;

            }

            if (this.NewPassword != this.ConfirmPassword)
            {

                await Application.Current.MainPage.DisplayAlert("Erro", "As palavras-chave não coincidem!", "Ok");

                return;

            }

            if (this.NewPassword == this.Password || this.ConfirmPassword == this.Password)
            {

                await Application.Current.MainPage.DisplayAlert("Erro", "A nova palavra-chave não pode ser igual à original!", "Ok");

                return;

            }

            var connection = await this._api.CheckConnection();

            if (!connection.IsSuccess)
            {

                await Application.Current.MainPage.DisplayAlert("Erro", connection.Message, "Ok");

                await Application.Current.MainPage.Navigation.PopAsync();

                return;

            }

            var client = new HttpClient();
            client.BaseAddress = new Uri("http://rcmeteorapi.azurewebsites.net/");

            try
            {

                var change = await client.PostAsync("api/Account/SetPassword", 
                    new StringContent(string.Format(
                    "NewPassword={0}&ConfirmPassword={1}",
                    NewPassword, ConfirmPassword),
                    Encoding.UTF8, "application/x-www-form-urlencoded"));

                if (!change.IsSuccessStatusCode)
                {

                    await Application.Current.MainPage.DisplayAlert("Erro", "Ocorreu um erro ao mudar a palavra-chave, por favor tente novamente mais tarde!", "Ok");

                    return;

                }

                this.IsVisible = false;

                this._main.Login.Password = NewPassword;

                await Application.Current.MainPage.Navigation.PopModalAsync();

                await Application.Current.MainPage.DisplayAlert("Sucesso!", "Palavra-chave mudada com sucesso!", "Ok");

            }
            catch
            {

                await Application.Current.MainPage.DisplayAlert("Erro", "Ocorreu um erro ao mudar a palavra-chave, por favor tente novamente mais tarde!", "Ok");

                return;

            }

        }

        private void Visibility()
        {
            this.IsVisible = true;
        }

        #endregion

    }

}
