﻿namespace RC_Meteor.ViewModels
{

    using System.Collections.Generic;
    using Models;
    using RC_Meteor.Helpers;
    using RC_Meteor.Services;

    public class MainViewModel : BaseViewModel
    {

        #region Properties - ViewModels

        /// <summary>
        /// Instancia a ViewModel Login.
        /// </summary>
        public LoginViewModel Login { get; set; }

        /// <summary>
        /// Instancia a ViewModel Register.
        /// </summary>
        public RegisterViewModel Register { get; set; }

        /// <summary>
        /// Instancia a ViewModel dos Paises.
        /// </summary>
        public CountriesViewModel Countries { get; set; }

        /// <summary>
        /// Instancia a ViewModel do perfil.
        /// </summary>
        public ProfileViewModel Profile { get; set; }

        /// <summary>
        /// Instancia a ViewModel das cidades.
        /// </summary>
        public CitiesViewModel Cities { get; set; }

        /// <summary>
        /// Instancia a ViewModel das previsões.
        /// </summary>
        public ForecastViewModel Forecasts { get; set; }

        #endregion

        #region Properties

        /// <summary>
        /// Obtem os dados necessários para se efetuar login.
        /// </summary>
        public TokenResponse Token { get; set; }

        /// <summary>
        /// Chave da API necessária para buscar os dados do tempo da cidade em si.
        /// </summary>
        public string APIKey { get; set; }

        /// <summary>
        /// Chave da API necessária para buscar os dados dos paises e respetivas cidades.
        /// </summary>
        public string XMashapeKey { get; set; }

        /// <summary>
        /// Guarda os dados de todos os paises.
        /// </summary>
        public List<Country> CountriesList { get; set; }

        /// <summary>
        /// Guarda os dados das cidades do pais respetivo.
        /// </summary>
        public List<City> CitiesList { get; set; }

        /// <summary>
        /// Guarda os dados do tempo durante os próximos cinco dias de um determinado país.
        /// </summary>
        public PrincipalForecast Forecast { get; set; }

        /// <summary>
        /// Usada para efeitos de BD Local.
        /// </summary>
        public DataService DataService { get; set; }

        #endregion

        #region Constructors

        public MainViewModel()
        {

            _instance = this;
            this.APIKey = "0925d6762b9b8780cb7552e9d5b57a9c";
            this.XMashapeKey = "uH2gukN7sjmshzEfnH7XLrgGbZ7Up1ULPgFjsneC9ZJfoMZz4d";
            this.Login = new LoginViewModel();
            this.DataService = new DataService();
        }

        #endregion

        #region Singleton

        private static MainViewModel _instance;

        public static MainViewModel GetInstance()
        {

            if (_instance == null)
            {

                return new MainViewModel();

            }

            return _instance;

        }

        #endregion

    }

}
