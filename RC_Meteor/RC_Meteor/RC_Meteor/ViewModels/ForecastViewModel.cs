﻿using GalaSoft.MvvmLight.Command;
using RC_Meteor.Models;
using RC_Meteor.Services;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using System.Globalization;
using Xamarin.Forms;

namespace RC_Meteor.ViewModels
{

    public class ForecastViewModel : BaseViewModel
    {

        #region Attributes

        private MainViewModel _main;
        private ApiService _api;
        private List _day1, _day2, _day3, _day4, _day5;
        private bool _isRunning;
        #endregion

        #region Properties

        public PrincipalForecast Forecast { get; set; }

        public List Day1
        {
            get { return this._day1; }
            set { SetValue(ref this._day1, value); }
        }

        public List Day2
        {
            get { return this._day2; }
            set { SetValue(ref this._day2, value); }
        }

        public List Day3
        {
            get { return this._day3; }
            set { SetValue(ref this._day3, value); }
        }

        public List Day4
        {
            get { return this._day4; }
            set { SetValue(ref this._day4, value); }
        }

        public List Day5
        {
            get { return this._day5; }
            set { SetValue(ref this._day5, value); }
        }

        public bool IsRunning
        {
            get { return this._isRunning; }
            set { SetValue(ref this._isRunning, value); }
        }

        #endregion

        #region Constructor

        public ForecastViewModel()
        {
            this.IsRunning = false;

            this._main = MainViewModel.GetInstance();
            this._api = new ApiService();
            this.Forecast = this._main.Forecast;
            this.LoadList();

        }

        #endregion

        #region Commands

        public ICommand RefreshCommand
        {
            get { return new RelayCommand(LoadList); }
        }

        #endregion

        #region Methods

        private async void LoadList()
        {

            this.IsRunning = true;

            this.Day1 = this.Day2 = this.Day3 = this.Day4 = this.Day5 = new List();

            this.Day1 = (from d in this.Forecast.List where DateTime.Parse(d.DateTimeString).Date == DateTime.Now.Date select d).First();
            this.Day2 = (from d in this.Forecast.List where DateTime.Parse(d.DateTimeString).Date == DateTime.Now.AddDays(1).Date select d).First();
            this.Day3 = (from d in this.Forecast.List where DateTime.Parse(d.DateTimeString).Date == DateTime.Now.AddDays(2).Date select d).First();
            this.Day4 = (from d in this.Forecast.List where DateTime.Parse(d.DateTimeString).Date == DateTime.Now.AddDays(3).Date select d).First();
            this.Day5 = (from d in this.Forecast.List where DateTime.Parse(d.DateTimeString).Date == DateTime.Now.AddDays(4).Date select d).First();

            this.IsRunning = false;

        }

        #endregion

    }

}
