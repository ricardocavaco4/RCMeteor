﻿using GalaSoft.MvvmLight.Command;
using RC_Meteor.Models;
using RC_Meteor.Services;
using RC_Meteor.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace RC_Meteor.ViewModels
{

    public class CountriesViewModel : BaseViewModel
    {

        #region Attributes

        private MainViewModel _main;
        private ApiService _api;
        private string _filter;
        private bool _isRefreshing;
        private ObservableCollection<Country> _countries;

        #endregion

        #region Properties

        public bool IsRefreshing
        {
            get { return this._isRefreshing; }
            set { SetValue(ref this._isRefreshing, value); }
        }

        public string Filter
        {
            get { return this._filter; }
            set
            {
                SetValue(ref this._filter, value);
                this.Search();
            }
        }

        public ObservableCollection<Country> Countries
        {
            get { return this._countries; }
            set { SetValue(ref this._countries, value); }
        }

        #endregion

        #region Constructor

        public CountriesViewModel()
        {

            this._main = MainViewModel.GetInstance();
            this._api = new ApiService();
            this.LoadCountries();

        }

        #endregion

        #region Commands

        public ICommand RefreshCommand
        {
            get { return new RelayCommand(LoadCountries); }
        }

        public ICommand SearchCommand
        {
            get { return new RelayCommand(Search); }
        }

        public ICommand LogoutCommand
        {
            get { return new RelayCommand(Logout); }
        }

        public ICommand ProfileCommand
        {
            get { return new RelayCommand(Profile); }
        }

        #endregion

        #region Command Methods

        private void Search()
        {

            if (string.IsNullOrWhiteSpace(this.Filter))
            {
                this.Countries = new ObservableCollection<Country>(this.ToCountry());
            }
            else
            {
                this.Countries = new ObservableCollection<Country>(this.ToCountry().Where
                (c => c.Name.ToLower().Contains(this.Filter.ToLower()) ||
                 c.Capital.ToLower().Contains(this.Filter.ToLower()) || c.Region.ToLower().Contains(this.Filter.ToLower())
                ));
            }

        }

        private async void Logout()
        {

            var connection = await this._api.CheckConnection();

            if (!connection.IsSuccess)
            {

                await Application.Current.MainPage.DisplayAlert("Erro", connection.Message, "Ok");

                await Application.Current.MainPage.Navigation.PopAsync();

                return;

            }


            var msg = await Application.Current.MainPage.DisplayActionSheet("Tem a certeza?", "Cancelar", null, new string[] { "Sim", "Não" });

            if(msg == "Sim")
            {

                var client = new HttpClient();
                client.BaseAddress = new Uri("http://rcmeteorapi.azurewebsites.net/");

                try
                {

                    var logout = await client.PostAsync("api/Account/Logout", null);

                    await Application.Current.MainPage.Navigation.PopToRootAsync();

                }
                catch
                {

                    await Application.Current.MainPage.DisplayAlert("Erro", "Não foi possivél efetuar o logout", "Ok");

                }

            }
            else
            {

                return;

            }         

        }

        private async void Profile()
        {

            this._main.Profile = new ProfileViewModel();

            await Application.Current.MainPage.Navigation.PushModalAsync(new ProfileModalPage());

        }

        #endregion

        #region Methods

        private IEnumerable<Country> ToCountry()
        {

            return _main.CountriesList.Select(c => new Country
            {

                Alpha2Code = c.Alpha2Code,
                Capital = c.Capital,
                Name = c.Name,
                NumericCode = c.NumericCode,
                Region = c.Region,

            });

        }

        private async void LoadCountries()
        {

            IsRefreshing = true;

            var connection = await this._api.CheckConnection();

            if (!connection.IsSuccess)
            {

                await Application.Current.MainPage.DisplayAlert("Erro", connection.Message, "Ok");

                await Application.Current.MainPage.Navigation.PopAsync();

                return;

            }


            var response = await this._api.GetList<Country>("http://restcountries.eu", "/rest", "/v2/all");

            if (!response.IsSuccess)
            {

                await Application.Current.MainPage.DisplayAlert("Erro", response.Message, "Ok");
                return;

            }

            MainViewModel.GetInstance().CountriesList = (List<Country>)response.Result;

            this.Countries = new ObservableCollection<Country>(this.ToCountry());
            IsRefreshing = false;

        }

        #endregion

    }

}
