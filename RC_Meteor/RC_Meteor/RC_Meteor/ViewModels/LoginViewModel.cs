﻿namespace RC_Meteor.ViewModels
{

    using GalaSoft.MvvmLight.Command;
    using RC_Meteor.Services;
    using RC_Meteor.Views;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class LoginViewModel : BaseViewModel
    {

        #region Attributes

        private string _email, _password;
        private MainViewModel _main;
        private ApiService _api;
        private bool _isRunning;

        #endregion

        #region Properties

        public string Email
        {
            get { return this._email; }
            set { SetValue(ref this._email, value); }
        }

        public string Password
        {
            get { return this._password; }
            set { SetValue(ref this._password, value); }
        }

        public bool IsRunning
        {
            get { return this._isRunning; }
            set { SetValue(ref this._isRunning, value); }
        }

        #endregion

        #region Constructor

        public LoginViewModel()
        {

            this._main = MainViewModel.GetInstance();
            this._api = new ApiService();
            this.IsRunning = false;
        }

        #endregion

        #region Commands

        public ICommand LoginCommand
        {
            get { return new RelayCommand(Login); }
        }

        public ICommand RegisterCommand
        {
            get { return new RelayCommand(Register); }
        }

        public ICommand AboutCommand
        {
            get { return new RelayCommand(About); }
        }

        #endregion

        #region Command Methods

        private async void Login()
        {

            if (string.IsNullOrEmpty(this.Email))
            {

                await Application.Current.MainPage.DisplayAlert("Erro", "Por favor introduza um email válido!", "OK");

                return;

            }

            if (string.IsNullOrEmpty(this.Password))
            {

                await Application.Current.MainPage.DisplayAlert("Erro", "Por favor introduza uma palavra-chave válida!", "OK");

                return;

            }

            this.IsRunning = true;

            var connection = await this._api.CheckConnection();

            if (!connection.IsSuccess)
            {

                this.IsRunning = false;

                await Application.Current.MainPage.DisplayAlert("Erro", "Por favor verifique a sua ligação à internet!", "OK");
                return;

            }

            var token = await this._api.GetToken("http://rcmeteorapi.azurewebsites.net", this.Email, this.Password);

            if (token == null)
            {

                this.IsRunning = false;

                await Application.Current.MainPage.DisplayAlert("Oh Não!", "Algum processo falhou, por favor tente mais tarde!", "OK");

                return;

            }

            if (string.IsNullOrEmpty(token.AccessToken))
            {

                this.IsRunning = false;

                await Application.Current.MainPage.DisplayAlert("Oh Não!", token.ErrorDescription, "OK");

                return;

            }

            var mainViewModel = MainViewModel.GetInstance();

            mainViewModel.Token = token;

            this.IsRunning = false;

            this._main.Countries = new CountriesViewModel();
            this._main.CountriesList = new List<Models.Country>();

            await Application.Current.MainPage.Navigation.PushAsync(new CountriesListPage());

        }

        private async void Register()
        {

            var connection = await this._api.CheckConnection();

            if (!connection.IsSuccess)
            {

                await Application.Current.MainPage.DisplayAlert("Erro", "Não foi possivél estabelecer uma ligação à internet, por favor tente mais tarde!", "Ok");

                return;

            }

            this._main.Register = new RegisterViewModel();

            await Application.Current.MainPage.Navigation.PushModalAsync(new RegisterModalWebPage());

        }

        private async void About()
        {

            await Application.Current.MainPage.Navigation.PushModalAsync(new AboutModalPage());

        }

        #endregion

    }

}
