﻿using System.Text;

namespace RC_Meteor.Services
{

    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Models;
    using Newtonsoft.Json;
    using Plugin.Connectivity;
    using Xamarin.Forms;

    public class ApiService
    {

        public async Task<Response> CheckConnection()
        {

            if (!CrossConnectivity.Current.IsConnected)
            {

                return new Response { IsSuccess = false, Message = "Please check your Internet configurations." };

            }

            var isReachable = await CrossConnectivity.Current.IsRemoteReachable("google.com");

            if (!isReachable)
            {

                return new Response { IsSuccess = false, Message = "Please check your Internet connection." };

            }

            return new Response { IsSuccess = true, Message = "Ok" };

        }

        public async Task<TokenResponse> GetToken(string urlBase, string username, string password)
        {

            try
            {

                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);

                var response = await client.PostAsync("Token",
                    new StringContent(
                        string.Format("grant_type=password&username={0}&password={1}", username, password),
                                      Encoding.UTF8,
                                      "application/x-www-form-urlencoded"));

                var resultJson = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<TokenResponse>(resultJson);

                return result;

            }
            catch
            {

                return null;

            }

        }

        public async Task<Response> GetList<T>(string urlBase, string servicePrefix, string controller)
        {

            try
            {

                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);

                var url = string.Format("{0}{1}", servicePrefix, controller);

                var response = await client.GetAsync(url);

                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response { Message = result, IsSuccess = false };
                }

                var list = JsonConvert.DeserializeObject<List<T>>(result);

                return new Response { IsSuccess = true, Result = list, Message = "Ok" };

            }
            catch (Exception ex)
            {
                return new Response { IsSuccess = false, Message = ex.Message };
            }

        }

        public async Task<Response> GetWeather<T>(string urlBase, string servicePrefix, string requested)
        {

            try
            {

                var client = new HttpClient();

                HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, $"{urlBase}{servicePrefix}{requested}");

                HttpResponseMessage responseMessage = await client.SendAsync(requestMessage);

                var result = await responseMessage.Content.ReadAsStringAsync();

                if (!responseMessage.IsSuccessStatusCode)
                {
                    return new Response { Message = result, IsSuccess = false };
                }                             
                
                var list = JsonConvert.DeserializeObject<T>(result);

                return new Response { IsSuccess = true, Result = list, Message = "Ok" };

            }
            catch (Exception ex)
            {
                return new Response { IsSuccess = false, Message = ex.Message };
            }

        }

    }

}
